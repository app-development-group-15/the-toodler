import React from 'react';
import { StyleSheet, View, ImageBackground } from 'react-native';
import NavBar from './src/components/NavBar';
import AppContainer from './src/routes';
const backgroundImage =  require('./src/resources/images/background.jpg');

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
  }
});

export default function App() {
  return (
    <AppContainer />
  );
}
 