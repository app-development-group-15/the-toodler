import React from 'react';
import { TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles';

// pass in properties
// navigation : "the navigation obj"
// action : "string for the equivelant name of a view"

class AddBtn extends React.Component {
  constructor(props) {
      super(props);
      console.log(this.route)
  }
  render () {
    const { action, navigation, paramsToSend } = this.props;
    return (
      <TouchableHighlight
        style={styles.AddBtn}
        activeOpacity={0.6}
        underlayColor="#DDDDDD"
        onPress={ () => navigation.navigate(action, paramsToSend) }
        >

        <Icon
          name="plus"
          size={30}
          color="white"
        />
      </TouchableHighlight>
    );
  }
}

export default AddBtn;
