import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  AddBtn: {
    width: 65,
    height: 65,
    backgroundColor: '#7F6EAF',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    position: 'absolute',
    bottom: 30,
    right: 30,
    alignSelf: 'flex-end',
  },
});
