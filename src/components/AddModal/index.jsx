import React from 'react';
import { Entypo } from '@expo/vector-icons';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity } from 'react-native';
import Modal from '../Modal';
import styles from './styles';

class AddModal extends React.Component {
    render() {
        const { isOpen, closeModal, takePhoto, selectFromCameraRoll } = this.props;
        return (
            <Modal
                isOpen={ isOpen }
                closeModal={ closeModal }>
                <TouchableOpacity
                    onPress={ () => takePhoto() }>
                    <Icon
                      name="camera"
                      size={80}
                      color="black"
                    />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={ () => selectFromCameraRoll() }>
                    <Icon
                      name="image"
                      size={80}
                      color="black"
                    />
                </TouchableOpacity>
            </Modal>
        );
    }
}

export default AddModal;
