import React from 'react';
import { View, Text, TouchableHighlight, Image, Alert } from 'react-native';
import ListView from '../../views/List/';
import styles from './styles.js';
import data from '../../resources/populate/data.json';

class BoardItem extends React.Component {
    countChildren(item){
        let counter = 0;
        data.lists.forEach(list => {
            if (item.id == list.boardId) {
                counter += 1;
            }
        }
        )
        return (counter);
    }
    onBoardLongPress(boardid) {
        Alert.alert(
            "Do You want to delete this board?",
            "This action cannot be undone",
            [
                {
                    text: "Cancel",
                    onpress: () => console.log("Item not deleted"),
                    style: "cancel"
                },
                {text: "Edit Board", onPress: () => this.editBoard()},
                { text: "Delete Board", onPress: () => this.deleteBoardItem(boardid) }
            ],
            { cancelable: false }
        )
    }

    editBoard(){
      console.log(this.props)
      const { navigation } = this.props;
      navigation.navigate('CreateBoard', {
        boards: data.boards,
        id: this.props.item.id,
        title: this.props.item.name,
        thumbnailPhoto: this.props.item.thumbnailPhoto
      })
    }

    deleteBoardItem(boardid) {
        // const index = this.props.lists.indexOf();
        // this.props.lists.filter(list=> list.id != listid);
        let index = this.props.boards.map(function (task) { return task.id }).indexOf(boardid);
        this.props.boards.splice(index, 1);
        this.forceUpdate()
        // data.lists.pop()
        // const{selectedLists} = this.state;

        // this.setState({
        //     state: this.state
        // })
        // console.log(this.state);
    }


    render() {
        const { item, navigation } = this.props;

        return (
            <View  >
                <TouchableHighlight  onPress={() => {
                    navigation.navigate('ListView', { board: item })}}
                    onLongPress = {() => this.onBoardLongPress(item.id)}
             >
                    <View style={styles.button}>
                        <View style={styles.textContainer}>
                            <Text style={styles.title} numberOfLines={2}>{item.name} </Text>

                        </View>
                        <View style={styles.image}>
                            <Image style={styles.image} source={{ uri: item.thumbnailPhoto }} resizeMode="cover" />
                        </View>
                        <Text style={styles.nrOfItems}>{this.countChildren(item)}</Text>
                </View>
                </TouchableHighlight>
            </View>
        )
    }
}

export default BoardItem;
