import { StyleSheet } from 'react-native';
import { item, text } from '../../styles/colors';

export default StyleSheet.create({
    button: {
        flexDirection: 'row',
        margin: 3,
        backgroundColor: item,
        borderRadius: 10,

    },
    title: {
        fontSize: 20,
        color: 'white',
    },
    image: {
        alignSelf: 'flex-end',
        marginLeft: 'auto',
        width: 100,
        height: 100,
        borderRadius: 10,
    },
    textContainer: {
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center',
    },
    nrOfItems: {
        //flexWrap: 'wrap',
        position: "absolute",
        top: 45,
        left: 10,
        color: "white",
        paddingTop: 20,
      },
}
)
