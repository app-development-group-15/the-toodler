import React from 'react';
import { View, FlatList, Text, TouchableHighlight } from 'react-native';
import ListItem from '../../components/BoardItem';
import listStyles from '../../styles/listPadding';


const BoardItems = ({ boards, navigation }) => (
    <View style={listStyles.list}>
        <FlatList
            numColumns={1}
            data={boards}
            renderItem={({ item }) => {
                return (
                    <ListItem item={item} navigation={navigation} boards={boards}/>
                )
            }
            }
            keyExtractor={(board) => board.id.toString()}
        />
    </View>
);
export default BoardItems;