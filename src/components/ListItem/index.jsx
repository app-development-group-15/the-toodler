import React from 'react';
import { View, Text, TouchableHighlight, Alert } from 'react-native';
import { NavigationEvents } from '@react-navigation/native';
import TaskView from '../../views/Task/';
import styles from './styles.js';
import { func } from 'prop-types';

class ListItem extends React.Component{
    onListLongPress(listid){
        Alert.alert(
            "Do You want to delete this list?",
            "This action cannot be undone",
            [
                {
                    text: "Cancel",
                    onpress:() => console.log("Item not deleted"),
                    style: "cancel"
                },
                {text: "Edit list", onPress: () => this.editList()},
                {text: "Delete list", onPress: () => this.deleteListItem(listid)}
            ],
            {cancelable: false}
        )
    }

    editList(){
      console.log(this.props)
      const { navigation } = this.props;
      navigation.navigate('CreateList', {
        lists: this.props.lists,
        connectedBoard: {id: this.props.boardid},
        id: this.props.item.id,
        title: this.props.item.name,
        color: this.props.item.color
      })
    }

    deleteListItem(listid){
        let index = this.props.lists.map(function (list) { return list.id}).indexOf(listid);
        this.props.lists.splice(index,1);
        this.forceUpdate()
    }



    render(){

        const {item, navigation} = this.props;
        if(item.boardId == this.props.boardid){

            return (
                <View style={[styles.item, { backgroundColor: this.props.item.color }]} >
                    <TouchableHighlight style={styles.button} onLongPress={() => this.onListLongPress(item.id)}
                    onPress={() => {
                        navigation.navigate('TaskView', { list: item })
                    }} >
                        <Text style={styles.title} numberOfLines={2}>{item.name} </Text>
                    </TouchableHighlight>
                </View>
            )
        }
        else {
            return (
                <></>
            )
        }
    }

}

export default ListItem;
