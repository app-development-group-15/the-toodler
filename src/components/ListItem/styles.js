import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    button: {
        margin: 3,
        alignItems: 'center',
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 20,
        paddingRight: 20,
      
    },
    item: {
        margin: 3,
        borderRadius: 5,

    },
    title: {
        fontSize: 20,
        color: 'white',
    }
}
)
