import React from 'react';
import { View, FlatList, Text, TouchableHighlight } from 'react-native';
import styles from './styles';
import ListItem from '../../components/ListItem';
import listStyles from '../../styles/listPadding';


const ListItems = ({ lists, navigation, boardid}) => (
    <View style={listStyles.list}>
        <FlatList
        numColumns={1}
        data={lists}
        renderItem={({item}) => {
            return(
                <ListItem item={item} navigation={navigation} boardid={boardid} lists={lists}/>
            )
        }
    }
    keyExtractor={(list) => list.id.toString()}
        />
    </View>
);
export default ListItems;