import React from 'react';
import { View, Text, TouchableHighlight, Alert } from 'react-native';
import styles from './styles.js';

class TaskItem extends React.Component {

    onTaskLongPress(taskid) {
        Alert.alert(
            "Do You want to delete this task?",
            "This action cannot be undone",
            [
                {
                    text: "Cancel",
                    onpress: () => console.log("Item not deleted"),
                    style: "cancel"
                },
                { text: "Delete task", onPress: () => this.deleteTaskItem(taskid) }
            ],
            { cancelable: false }
        )
    }

    deleteTaskItem(taskid) {
        // const index = this.props.lists.indexOf();
        // this.props.lists.filter(list=> list.id != listid);
        let index = this.props.tasks.map(function (task) { return task.id }).indexOf(taskid);
        this.props.tasks.splice(index, 1);
        this.forceUpdate()
     
    }

    render(){
        const{item, listid, tasks} = this.props;
        if(item.listId == listid){
            return(
            <View >
                <TouchableHighlight onLongPress={() => this.onTaskLongPress(item.id)}>
                  
                    <View style={styles.button}>
                        <Text style={styles.name} numberOfLines={1}>
                            {item.name}
                        </Text>
                        <Text style={styles.description} numberOfLines={3}>
                            {item.description}
                        </Text>
                        {
                            (item.isFinished)
                            ?
                            <Text style={styles.taskCompletion}>Finished</Text>
                            :
                            <Text style={styles.taskCompletion}>Not finished</Text>
                            }
                    </View>
                </TouchableHighlight>
            </View>
            
            )
        }
        else{
            return(
                <></>
            )
        }   
    }
};

export default TaskItem;