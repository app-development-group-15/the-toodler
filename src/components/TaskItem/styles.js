import { StyleSheet } from 'react-native';
import {item, text} from '../../styles/colors';
//import { block } from 'react-native-reanimated';



export default StyleSheet.create({
    name: {
     
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        fontSize: 20,
        color: 'white',
    },

    button: {
        flex: 1,
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 20,
        paddingRight: 20,
        // Fix color
        borderRadius: 10,
        margin: 1,
        fontSize: 20,
        color: 'white',
        backgroundColor: item,


    },
    description: {
        paddingTop: 10,
        paddingBottom: 10,
        color: 'white',
        fontSize: 16,
        flexDirection: 'row',
    },
    taskCompletion: {
        color: 'white',
    }
});