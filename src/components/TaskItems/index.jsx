import React from 'react';
import { View, FlatList, Text } from 'react-native';
import TaskItem from '../TaskItem';
//import styles from './styles';
import listStyles from '../../styles/listPadding';

// Props are tasks and listid
const TaskItems = ({ list, tasks }) => (
    console.log("TASKITEMS"),
    <View style={listStyles.list}>
        <FlatList
            numColumns={1}
            data={tasks}
            renderItem={({ item}) => {
                return(
                    <TaskItem item={item} listid={list.id} tasks={tasks}/>    
                )
            }
            }
            keyExtractor={(task) => task.id.toString()}
        />
    </View>
);
export default TaskItems;