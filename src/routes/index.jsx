import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Main from '../views/DemoFront';
import TestNav from '../views/DemoNavigate';
import ListView from '../views/List';
import TaskView from '../views/Task';
import BoardView from '../views/Board';
import CreateBoard from '../views/CreateBoard';
import CreateList from '../views/CreateList';
import CreateTask from '../views/CreateTask';

const Stack = createStackNavigator();

function AppContainer() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="BoardView" component={BoardView} />
        <Stack.Screen name="ListView" component={ListView} />
        <Stack.Screen name="TaskView" component={TaskView} />
        <Stack.Screen name="Main" component={Main} />
        <Stack.Screen name="TestNav" component={TestNav} />
        <Stack.Screen name="CreateBoard" component={CreateBoard} />
        <Stack.Screen name="CreateList" component={CreateList} />
        <Stack.Screen name="CreateTask" component={CreateTask} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default AppContainer;
