import * as FileSystem from 'expo-file-system';

const imageDirectory = `${FileSystem.documentDirectory}images`;

const onException = (cb, errorHandler) => {
    try {
        return cb();
    } catch (err) {
        if (errorHandler) {
            return errorHandler(err);
        }
        console.error(err);
    }
}

export const copyFile = async (file, newLocation) => {
  return await onException(() => FileSystem.copyAsync({
    from: file,
    to: newLocation
  }));
}

const loadImage = async (fileName) => {
  return FileSystem.readAsStringAsync(`${imageDirectory}/${fileName}`, {
    encoding: FileSystem.EncodingType.Base64
  });
}

export const addImage = async (imageLocation) => {
  const folderSplit = imageLocation.split('/');
  const fileName = folderSplit[folderSplit.length -1];
  console.log('copy file');
  await onException(() => copyFile(imageLocation, `${imageDirectory}/${fileName}`));
  console.log('return file')
  return {
    name: fileName,
    file: loadImage(fileName),
  }
}

const setupDirectory = async () => {
  const dir = await FileSystem.getInfoAsync(imageDirectory);
  if(!dir.exists){
    await FileSystem.makeDirectoryAsync(imageDirectory);
  }
}

export const getAllImages = async () => {
  await setupDirectory();

  const result = await FileSystem.readDirectoryAsync(imageDirectory);
  return Promise.all(result.map(async (fileName) => {
    return {
      name: fileName,
      type: 'image',
      file: await loadImage(fileName)
    }
  }))
}
