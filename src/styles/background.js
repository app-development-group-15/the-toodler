import { StyleSheet } from 'react-native';


export default StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
    },
    image: {
        flex: 1,
        resizeMode: "cover",
    }
});