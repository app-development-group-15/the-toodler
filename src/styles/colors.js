export const lightPurple = '#7F6EAF';
export const fieldBackground = '#C4C4C4';
export const selectedInputColor = '#2C9FF5';
export const text = '#868686';
export const item = '#484848';
