import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    list: {
        flex: 1,
        paddingTop: 30,
        paddingLeft: 20,
        paddingRight: 20,
    },
});