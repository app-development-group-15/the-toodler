import React from 'react';
import { View, ImageBackground, Button } from 'react-native';
import data from '../../resources/populate/data.json';
import BoardItems from '../../components/BoardItems';
import AddBtn from '../../components/AddBtn';
import backgrStyle from '../../styles/background';
const backgroundImage = require('../../resources/images/background.jpg');


class BoardView extends React.Component {
    constructor(props) {
        super(props);
        console.log(this.props.route)
    }
    componentDidMount() {

        this.props.navigation.setOptions({
            // Pass in board name
            title: 'Boards',

        })
    }
    render() {
        if(this.props.data){
          const { data } = this.props;
        }
        const board = 'CreateBoard';
        return (
            <View style={{ flex: 1 }}>
                <ImageBackground source={backgroundImage} style={backgrStyle.image}>
                    <BoardItems boards={data.boards} navigation={this.props.navigation} />
                    <AddBtn
                        action={board}
                        navigation={this.props.navigation}
                        paramsToSend={{boards: data.boards}}
                    />
                </ImageBackground>
            </View>
        )
    }
}

export default BoardView;
