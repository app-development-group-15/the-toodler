import React from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  TouchableHighlight,
  StyleSheet,
  Button,
  ImageBackground
} from 'react-native';
import AddModal from '../../components/AddModal';
import styles from './styles';
import {StackActions} from '@react-navigation/native';

class CreateBoard extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props);
  }

  //functions for text inputs
  state = {
      //user input params
      title: '',
      description: '',
      imageUrl: '',
  }
  createBoard(){
    let boards = this.props.route.params.boards;
    let newBoardId = boards[boards.length -1].id + 1;
    boards.push({
      id: newBoardId,
      name: this.state.title,
      description: this.state.description,
      thumbnailPhoto: this.state.image
    })
    console.log('Added new board with id: ' + newBoardId);
    this.props.navigation.goBack()
  }

  updateBoard(id){
    let boards = this.props.route.params.boards;
    let index = boards.map(function(board) { return board.id }).indexOf(id);

    boards[index] = {
      id: id,
      name: this.state.title,
      description: this.state.description,
      thumbnailPhoto: this.state.thumbnailPhoto
    }
    this.props.navigation.dispatch(
      StackActions.popToTop()
    );
    this.forceUpdate();
  }

    render () {
      const { id, title, description, thumbnailPhoto } = this.props.route.params;

      return (
            <View style={styles.container}>
              {/* Create Title field */}
              <View style={styles.titleView}>
                <Text style={styles.title}>
                  Title:
                </Text>
                <TextInput
                  onFocus={ this.onFocusChange }
                  onBlur={ this.onFocusChange }
                  ref={(el) => { this.title = el }}
                  onChangeText={(title) => this.setState({title})}
                  style={styles.titleInput}
                  defaultValue={title}
                  placeholder='My Amazing Board'>
                </TextInput>
              </View>

              <View style={styles.descriptionView}>
                <Text style={styles.title}>
                  Description:
                </Text>
                <TextInput
                  ref={(el) => { this.description = el }}
                  onChangeText={(description) => this.setState({description})}
                  style={styles.descriptionTextInput}
                  multiline={true}
                  defaultValue={description}
                  placeholder='My Amazing Board'>
                </TextInput>
              </View>

              <View style={styles.titleView}>
                <Text style={styles.title}>
                  Image Url:
                </Text>
                <TextInput
                  ref={(el) => { this.image = el }}
                  onChangeText={(image) => this.setState({image})}
                  style={styles.titleInput}
                  defaultValue={thumbnailPhoto}
                  placeholder='Paste url here'>
                </TextInput>
              </View>
              <View style={styles.imageBar}>
                <TouchableHighlight
                  style={styles.addImageBtn}
                  onPress={() => {typeof id == 'undefined' ? this.createBoard() : this.updateBoard(id)}}>
                  {typeof id == 'undefined' ?
                    <Text style={styles.addImageText}>Create List</Text>
                    :
                    <Text style={styles.addImageText}>Update List</Text>
                  }
                  </TouchableHighlight>
              </View>
            </View>
      );
    }
}

export default CreateBoard
