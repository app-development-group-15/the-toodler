import React from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  TouchableHighlight,
  StyleSheet,
  Button,
  ImageBackground
} from 'react-native';
import AddModal from '../../components/AddModal';
import styles from './styles';

class CreateList extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props);
  }

  //functions for text inputs
  state = {
      //user input params
      title: '',
      description: '',
      imageUrl: '',
  }

  componentDidMount(){
    this.setActiveColorState(this.props.route.params.color);
  }

  getActiveColor(){
    if(this.state.red) return '#AA4646';
    else if(this.state.blue) return '#6E90AF';
    else if(this.state.green) return '#6EAFA0';
    else if(this.state.yellow) return '#AAAF6E';
    else return '#484848';
  }

  setActiveColorState(code){
    if(code == '#AA4646') this.setState({red: true});
    else if(code == '#6E90AF') this.setState({blue: true});
    else if(code == '#6EAFA0') this.setState({green: true});
    else if(code == '#AAAF6E') this.setState({yellow: true});
  }

  updateList(listId){
    let lists = this.props.route.params.lists;
    let index = lists.map(function(list) {return list.id}).indexOf(listId);
    lists[index] = {
      id: listId,
      name: this.state.title,
      color: this.getActiveColor(),
      boardId: this.props.route.params.connectedBoard.id
    };
    console.log('Updated list with id: ' + listId);
    this.props.navigation.goBack();
  }

  createList(){
  let lists = this.props.route.params.lists;
  let newListId = lists[lists.length -1].id + 1;
  lists.push({
    id: newListId,
    name: this.state.title,
    color: this.getActiveColor(),
    boardId: this.props.route.params.connectedBoard.id
  })
  console.log('Added new list with id: ' + newListId);
  this.props.navigation.goBack()
}

    render () {
      const { id, title, color } = this.props.route.params;

      return (
            <View style={styles.container}>
              {/* Create Title field */}
              <View style={styles.titleView}>
                <Text style={styles.title}>
                  Title:
                </Text>
                <TextInput
                  onFocus={ this.onFocusChange }
                  onBlur={ this.onFocusChange }
                  ref={(el) => { this.title = el }}
                  onChangeText={(title) => this.setState({title})}
                  style={styles.titleInput}
                  defaultValue={title}
                  placeholder='My Amazing List'>
                </TextInput>
              </View>

              <View style={styles.titleView}>
                <Text style={styles.title}>
                  Pick a color:
                </Text>

                <View style={styles.colorContainer}>
                  <TouchableHighlight
                    style={(this.state.red) ? styles.colorRedSelected : styles.colorRed}
                    onPress={() => this.setState({red: true, blue: false, green: false, yellow: false})}>
                    <View></View>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={(this.state.blue) ? styles.colorBlueSelected : styles.colorBlue}
                    onPress={() => this.setState({red: false, blue: true, green: false, yellow: false})}>
                    <View></View>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={(this.state.yellow) ? styles.colorYellowSelected : styles.colorYellow}
                    onPress={() => this.setState({red: false, blue: false, green: false, yellow: true})}>
                    <View></View>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={(this.state.green) ? styles.colorGreenSelected : styles.colorGreen}
                    onPress={() => this.setState({red: false, blue: false, green: true, yellow: false})}>
                    <View></View>
                  </TouchableHighlight>
                </View>

              </View>

              <View style={styles.imageBar}>
                  <TouchableHighlight
                    style={styles.addImageBtn}
                    onPress={() => {typeof id == 'undefined' ? this.createList() : this.updateList(id)}}>
                    {typeof id == 'undefined' ?
                      <Text style={styles.addImageText}>Create List</Text>
                      :
                      <Text style={styles.addImageText}>Update List</Text>
                    }
                    </TouchableHighlight>
              </View>
            </View>
      );
    }
}

export default CreateList
