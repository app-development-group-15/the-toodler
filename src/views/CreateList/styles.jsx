import { StyleSheet, Dimensions } from 'react-native';
import { fieldBackground, selectedInputColor } from '../../styles/colors';

const { width: winWidth, height: winHeight } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    backgroundColor: '#403D3D',
    flex: 1,
  },
  background: {
    flex: 1,
    resizeMode: "cover",
  },
  titleView: {
    backgroundColor:fieldBackground,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    marginBottom: 5,
    height: 80,
  },
  title: {
    color: 'black',
    fontSize: 18,
  },
  titleInput: {
    height: 40,
    width: 250,
    backgroundColor: '#8A8A8A',
    borderRadius: 10,
    textAlign: 'center',
    marginLeft: 'auto',
  },
  descriptionView: {
    backgroundColor:fieldBackground,
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20,
    marginBottom: 5,
    height: 300,
  },
  descriptionTextInput: {
    backgroundColor: '#8A8A8A',
    borderRadius: 15,
    height: 240,
    padding: 25,
    paddingTop: 10,
  },
  imageBar: {
    backgroundColor:fieldBackground,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    height: 100,
  },
  addImageBtn: {
    backgroundColor: '#363636',
    padding: 10,
    borderRadius: 5,
  },
  addImageText: {
    color: 'white',
    fontSize: 24,
  },
  colorContainer: {
    flexDirection: 'row',
    paddingLeft: 20,
    paddingRight: 20
  },
  colorRedSelected: {
    borderRadius: 50,
    width: 25,
    height: 25,
    borderWidth: 3,
    margin: 15,
    borderColor: '#5BB7EB',
    backgroundColor: '#AA4646'
  },
  colorRed: {
    borderRadius: 50,
    width: 25,
    height: 25,
    margin: 15,
    backgroundColor: '#AA4646'
  },
  colorBlueSelected: {
    borderRadius: 50,
    width: 25,
    height: 25,
    borderWidth: 3,
    margin: 15,
    borderColor: '#5BB7EB',
    backgroundColor: '#6E90AF'
  },
  colorBlue: {
    borderRadius: 50,
    width: 25,
    height: 25,
    margin: 15,
    backgroundColor: '#6E90AF'
  },
  colorGreenSelected: {
    borderRadius: 50,
    width: 25,
    height: 25,
    borderWidth: 3,
    margin: 15,
    borderColor: '#5BB7EB',
    backgroundColor: '#6EAFA0'
  },
  colorGreen: {
    borderRadius: 50,
    width: 25,
    height: 25,
    margin: 15,
    backgroundColor: '#6EAFA0'
  },
  colorYellowSelected: {
    borderRadius: 50,
    width: 25,
    height: 25,
    borderWidth: 3,
    margin: 15,
    borderColor: '#5BB7EB',
    backgroundColor: '#AAAF6E'
  },
  colorYellow: {
    borderRadius: 50,
    width: 25,
    height: 25,
    margin: 15,
    backgroundColor: '#AAAF6E'
  }
});
