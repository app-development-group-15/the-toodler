import React from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  TouchableHighlight,
  StyleSheet,
  Button,
  ImageBackground
} from 'react-native';
import AddModal from '../../components/AddModal';
import styles from './styles';

class CreateTask extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props);
  }

  //functions for text inputs
  state = {
      //user input params
      title: '',
      description: '',
      isFinished: false,
  }
  createTask(){
    let tasks = this.props.route.params.data.tasks;
    let newTaskId = tasks[tasks.length -1].id + 1;
    tasks.push({
      id: newTaskId,
      name: this.state.title,
      description: this.state.description,
      isFinished: this.state.isFinished,
      listId: this.props.route.params.connectedList.id
    })
    console.log('Added new task with id: ' + newTaskId);
    this.props.navigation.goBack()
  }
    render () {
      const { title, description, isFinished } = this.props;
      return (
            <View style={styles.container}>
              {/* Create Title field */}
              <View style={styles.titleView}>
                <Text style={styles.title}>
                  Title:
                </Text>
                <TextInput
                  onFocus={ this.onFocusChange }
                  onBlur={ this.onFocusChange }
                  ref={(el) => { this.title = el }}
                  onChangeText={(title) => this.setState({title})}
                  style={styles.titleInput}
                  value={title}
                  placeholder='My Amazing Task'>
                </TextInput>
              </View>

              <View style={styles.descriptionView}>
                <Text style={styles.title}>
                  Description:
                </Text>
                <TextInput
                  ref={(el) => { this.description = el }}
                  onChangeText={(description) => this.setState({description})}
                  style={styles.descriptionTextInput}
                  multiline={true}
                  value={description}
                  placeholder='My Amazing task description'>
                </TextInput>
              </View>

              <View style={styles.imageBar}>
                  <TouchableHighlight
                    style={styles.addImageBtn}
                    onPress={() => this.createTask()}>
                      <Text style={styles.addImageText}>
                        Create Task
                      </Text>
                    </TouchableHighlight>
              </View>
            </View>
      );
    }
}

export default CreateTask
