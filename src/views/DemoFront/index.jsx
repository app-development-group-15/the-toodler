import React from 'react';
import { View, Text, StyleSheet, ImageBackground } from 'react-native';
import AddBtn from '../../components/AddBtn';
import backgrStyle from '../../styles/background';
import { useRoute } from '@react-navigation/native';


const backgroundImage =  require('../../resources/images/background.jpg');


const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
  }
});

class DemoFront extends React.Component {
  
  constructor(props) {
    super(props);
    console.log(this.props);
  }
    static navigationOptions = {
        title: 'Demo Front'
    }

    render () {
      const { navigation } = this.props;
      const pageToVisit = 'CreateBoard';
      return (
        <View style={styles.container}>
          <ImageBackground source={backgroundImage} style={backgrStyle.image}>
            <AddBtn
              action={ pageToVisit }
              navigation={this.props.navigation}
            />
          </ImageBackground>
        </View>
      );
    }
}

export default DemoFront
