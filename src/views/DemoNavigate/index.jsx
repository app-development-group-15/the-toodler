import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import AddBtn from '../../components/AddBtn';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
  }
});

class DemoNavigate extends React.Component {

    componentDidMount () {

      this.props.navigation.setOptions({
        headerRight: () => (
          <Button
            onPress={() => alert('Your Item has been edited')}
            title="Edit"
          />
        )
      })
    }
    render () {
      const { navigation } = this.props;
      return (
        <View style={styles.container}>
          <Text>This is a navigated to page</Text>
        </View>
      );
    }
}

export default DemoNavigate
