import React from 'react';
import { View, ImageBackground, Button } from 'react-native';
import data from '../../resources/populate/data.json';
import ListItems from '../../components/ListItems';
import AddBtn from '../../components/AddBtn';
import backgrStyle from '../../styles/background';
const backgroundImage = require('../../resources/images/background.jpg');


class ListView extends React.Component {
    // constructor(props) {
    //     super(props);
    //     console.log(this.props.route)
    // }
    componentDidMount() {

        this.props.navigation.setOptions({
            // Pass in board name
            title: this.props.route.params.board.name,

        })
    }
    render(){
      if(this.props.data){
        const { data } = this.props;
      }
        const createList = 'CreateList';
        const { board } = this.props.route.params;
        return (
            <View style={{flex: 1}}>
                <ImageBackground source={backgroundImage} style={backgrStyle.image}>
                <ListItems lists={data.lists}  navigation={this.props.navigation} boardid={board.id}/>
                <AddBtn
                    action={createList}
                    navigation={this.props.navigation}
                    paramsToSend={{connectedBoard: board, lists: data.lists}}
                />
                </ImageBackground>
            </View>
        )
    }
}

export default ListView;
