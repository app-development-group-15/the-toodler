import React from 'react';
import { View, Button, ImageBackground } from 'react-native';
import data from '../../resources/populate/data.json';
import TaskItems from '../../components/TaskItems';
import AddBtn from '../../components/AddBtn';
const backgroundImage = require('../../resources/images/background.jpg');
import backgrStyle from '../../styles/background';





class TaskView extends React.Component {
    constructor(props) {
        super(props);
        console.log("TASKVIEW")
    }
    componentDidMount() {

        this.props.navigation.setOptions({
            // Pass in board name
            title: this.props.route.params.list.name
        })
    }
    render() {
       const { list } = this.props.route.params
       const createTask = 'CreateTask';
        return (

            <View style={{ flex: 1 }}>
                <TaskItems
                  tasks={data.tasks}
                  list={list}/>
                <AddBtn
                    action={createTask}
                    navigation={this.props.navigation}
                    paramsToSend={{connectedList: list, data: data}}
                />
            </View>
        )
    }
}

export default TaskView;
